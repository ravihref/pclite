$(document).ready(function() {
    let renewChartDrawn = false;
    let orderChartDrawn = false;
    createTable(window.partnerInsights.renewalAjaxData);
    createTable(window.partnerInsights.orderAjaxData);
    google.charts.load('current', {'packages':['bar']});

    $('.reset-filter').on('click', function (e) {
        let container = $(e.target).attr('data-container');
        let selectFields = container === 'renewalContainer' ? window.partnerInsights.renewalSelects.selects : window.partnerInsights.orderSelects.selects;
        let inputFields = container === 'renewalContainer' ? window.partnerInsights.renewalSelects.inputs : window.partnerInsights.orderSelects.inputs;
        let tableObject =  container === 'renewalContainer' ? window.partnerInsights.renewalAjaxData : window.partnerInsights.orderAjaxData;
        let filterPresent = false;
        var table = $(`#${$(e.target).attr('data-table')}`).DataTable();
        table.search('').columns().search('').draw();

        selectFields.forEach((item) => {
            if ($(`#${item}`).val()) {
                $(`#${item}`).val('').trigger('change.select2');
                filterPresent = true;
            }

            if (['renEndUser','ordEndUser'].includes(item)) {
                $(`#${item}`).prop('disabled', false);
            }
        });

        let counter = 0;
        inputFields.forEach((item, index) => {
            if ($(`#${item}`).val()) {
                counter++;
                if (counter > 1) {
                    $(`#${item}`).val('').trigger('change');
                    filterPresent = false;
                } else {
                    $(`#${item}`).val('');
                }
            }
        });

        setTimeout(() => {
            if (filterPresent) {
                plotRevenueChart(tableObject);
                plotTotalOrderChart(tableObject);
            }
        }, 500);
    });

    $('.tablinks').on('click', function (e) {
        let i, tabcontent, tablinks;
        tabcontent = $('.tabcontent');
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = $('.tablinks');
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        $('#' + $(e.target).data('event')).show();
        e.currentTarget.className += " active";

        drawChartOnActiveTab();
    });

    for (let key in window.partnerInsights.select2IdsAndPlaceholders) {
        $(`#${key}`).select2({
            placeholder: window.partnerInsights.select2IdsAndPlaceholders[key]
        });
    }

    function drawChartOnActiveTab () {
        if ($('.tablinks.active').data('event') === 'renewalContainer' && !renewChartDrawn && $.fn.DataTable.isDataTable('#renewalTable')) {
            renewChartDrawn = true;
            plotRevenueChart(window.partnerInsights.renewalAjaxData);
            plotTotalOrderChart(window.partnerInsights.renewalAjaxData);
        } else if ($('.tablinks.active').data('event') === 'orderContainer' && !orderChartDrawn && $.fn.DataTable.isDataTable('#orderTable')) {
            orderChartDrawn = true;
            plotRevenueChart(window.partnerInsights.orderAjaxData);
            plotTotalOrderChart(window.partnerInsights.orderAjaxData);
        }
    }

    function appendFilterToDom (array, filter, tab, defaultSelect) {
        let html = ``;
        array.forEach(element => {
            if (element) {
                html += `<option value="${element}">${element}</option>`;
            }
        });

        if (filter in window.partnerInsights.filterAppendObject && !window.partnerInsights.filterAppendObject[filter].length) {
            window.partnerInsights.filterAppendObject[filter] = html;
        }

        $(`#${tab} #${filter}`).empty();
        $(`#${tab} #${filter}`).append(html);
    }

    function getColumnData (tableId, columnNo) {
        let arr = [];
        tableId.column(columnNo, {search:'applied'} ).data().each(function(value) {
            arr.push(value);
        });

        arr = [...new Set(arr)];
        return arr
    }

    function createTable (tableData) {
        $.ajax({
          url: tableData.url,
          type: "GET",
          beforeSend: function(xhr){xhr.setRequestHeader('ocp-apim-subscription-key', '6e21cd1f54e44e229729a635a137d0b6');},
          success: function(response) {
              $(`#${tableData.table}`).show();
              $(`.${tableData.spinner}`).hide();
              $(`#${tableData.error}`).html('');
              tableData.tableName = $(`#${tableData.table}`).DataTable( {
                  data: response.elements,
                  dom: 'lBfrtip',
                  buttons: [
                    'excel'
                  ],
                  ordering: true,
                  searching: true,
                  columns: tableData.columns
              });

              tableData.dropdown.forEach((element) => {
                appendFilterToDom(getColumnData(tableData.tableName, element.columnNo), element.id, tableData.container, element.columnName);
              });

              tableData.dropdown.forEach((element) => {
                  if ('linkedColumnId' in element) {
                    createLinkedDropdownEvent(tableData.tableName, tableData.container, element.id, element.columnNo, element.linkedColumnId, element.linkedColumnName, element.linkedColumnNo, window.partnerInsights.filterAppendObject[element.linkedColumnId]);
                  } else {
                    createDropdownEvent(tableData.tableName, tableData.container, element.id, element.columnNo);
                  }
              });

              expansionEvent (tableData.table, tableData.tableName, tableData.expansionEventType + 's');
              drawChartOnActiveTab();
              dateHandling(tableData.date.firstDate, tableData.date.lastDate, tableData.table, tableData.tableName);
          },
          error: function (jqXHR, exception) {
              $(`.${tableData.spinner}`).hide();
              $(`#${tableData.error}`).html(errorHandling(jqXHR, exception));
              $(`#${tableData.reload}`).show();
          }
        });
    }

    function expansionEvent (tableId, containerTable, type) {
        $(`#${tableId} tbody`).on('click', 'td.details-control', function () {
            let tr = $(this).closest('tr');
            let row = containerTable.row(tr);
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(expansionFormat(row.data(), type)).show();
                tr.addClass('shown');
            }
        });
    }

    function expansionFormat (rowData, orderType) {
        let div = $('<div/>').addClass('expansion-loading').html('Loading...');
        if (rowData.CustomerPO) {
            $.ajax( {
                url: `${window.partnerInsights.host}/ordersrenewal/rv_partner_central_lite_${orderType}_line_rbac?CustomerPO=${rowData.CustomerPO}`,
                type: "GET",
                beforeSend: function(xhr){xhr.setRequestHeader('ocp-apim-subscription-key', '6e21cd1f54e44e229729a635a137d0b6');},
                success: function (response) {
                    let childHtml = '';
                    if (orderType === 'orders') {
                        childHtml = '<table class="child-table"><thead><tr><th>SKU</th><th>SKU Description</th><th>Product Type</th><th>Quantity</th></tr></thead><tbody>';
                        response.elements.forEach((elem) => {
                            childHtml += `<tr><td>${elem.PartNumber}</td><td>${elem.PartDescription}</td><td>${elem.ProductType}</td><td>${elem.NumberofItemsQTY}</td></tr>`;
                        });
                    } else {
                        childHtml = '<table class="child-table"><thead><tr><th>Product SKU</th><th>Service SKU</th><th>Service SKU Description</th><th>Serial Number</th><th>Renewal Date</th></tr></thead><tbody>';
                        response.elements.forEach((elem) => {
                            childHtml += `<tr><td>${elem.ProductSKU}</td><td>${elem.ServiceSKU}</td><td>${elem.ServiceSKUDescription}</td><td>${elem.SerialNumber}</td><td>${elem.ExpirationDate ? moment(elem.ExpirationDate).format(window.partnerInsights.dateFormat) : ''}</td></tr>`;
                        });
                    }
                    childHtml += '</tbody></table>';
                    div.html(childHtml).removeClass('expansion-loading');
                },
                error: function (jqXHR, exception) {
                    div.html(errorHandling(jqXHR, exception)).removeClass('expansion-loading');
                }
            });
        } else {
            div.html('No Customer PO present').removeClass('expansion-loading');
        }
        return div;
    }

    function dateHandling (firstDate, lastDate, tableType, containerTable) {
        let minDate, maxDate;

        minDate = new DateTime($(`#${firstDate}`), {
            format: window.partnerInsights.dateFormat
        });
        maxDate = new DateTime($(`#${lastDate}`), {
            format: window.partnerInsights.dateFormat
        });

        $(`#${firstDate}, #${lastDate}`).on('change', function () {
            containerTable.draw();
        });

        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                if (settings.nTable.id !== tableType) {
                    return true;
                }

                let min = new Date($(`#${firstDate}`).val());
                let max = new Date($(`#${lastDate}`).val());

                if (!$(`#${firstDate}`).val().length || !$(`#${lastDate}`).val().length) {
                    min = null;
                    max = null;
                }

                let date;

                if (tableType === 'orderTable') {
                    date = new Date(data[8]);
                } else {
                    date = new Date(data[10]);
                }

                if ((min === null && max === null) || (min === null && date <= max) || (min <= date && max === null) || (min <= date && date <= max)) {
                    return true;
                }

                return false;
            }
        );
    }

    function errorHandling(jqXHR, exception) {
        let msg = '';
        if (jqXHR.status === 0) {
            msg = 'Not connect.\n Verify Network.';
        } else if (jqXHR.status == 404) {
            msg = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            msg = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            msg = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            msg = 'Time out error.';
        } else if (exception === 'abort') {
            msg = 'Ajax request aborted.';
        } else {
            msg = 'Uncaught Error.\n' + jqXHR.responseText;
        }

        return msg;
    }

    //Renewal Events
    function createDropdownEvent (table, container, columnName, columnNo) {
        $(`#${container} #${columnName}`).on('change', function () {
            let val = [];
            if ($(`#${columnName}`).val() != null) {
                $(`#${columnName}`).val().forEach((item) => {
                    val.push($.fn.dataTable.util.escapeRegex(item));
                });
                val = val.join('|');
                table.column(columnNo).search(val ? "^("+ val + ")$" : '', true, false).draw();
            } else {
                val = val.join('|');
                table.column(columnNo).search(val ? "^("+ val + ")$" : '', true, false).draw();
            }
        });
    }

    //Renewal Linked Events
    function createLinkedDropdownEvent (table, container, dropdown, columnNo, linkedColumn, linkedColumnName, linkedColumnNo, emptyFilterData) {
        $(`#${container} #${dropdown}`).on('change', function () {
            let val = [];
            if ($(`#${dropdown}`).val() != null) {
                $(`#${dropdown}`).val().forEach((item) => {
                    val.push($.fn.dataTable.util.escapeRegex(item));
                });
                val = val.join('|');
                table.column(columnNo).search(val ? "^("+ val + ")$" : '', true, false).draw();
            } else {
                val = val.join('|');
                table.column(columnNo).search(val ? "^("+ val + ")$" : '', true, false).draw();
            }

            if ($(`#${dropdown}`).val() === null) {
                $(`#${container} #${linkedColumn}`).empty().append(emptyFilterData).trigger('change');
            } else {
                appendFilterToDom(getColumnData(table, linkedColumnNo), linkedColumn, container, linkedColumnName);
            }

            let idPre = container === 'orderContainer' ? 'ord' : 'ren';
            if ($(`#${container} #${idPre}Country`).val() && $(`#${container} #${idPre}Partner`).val()) {
                $(`#${container} #${idPre}EndUser`).prop('disabled', false);
            } else if (!$(`#${container} #${idPre}Country`).val()) {
                $(`#${container} #${idPre}EndUser`).prop('disabled', false);
            } else {
                $(`#${container} #${idPre}EndUser`).prop('disabled', true);
            }
        });
    }

    $('.reload').on('click', (e) => {
        if ($(e.target).attr('id') === 'renewalReload') {
            createTable(window.partnerInsights.renewalAjaxData);
            $('#renewalReload').hide();
            $(`.renewalSpinner`).show();
        } else {
            createTable(orderAjaxData);
            $('#orderReload').hide();
            $('.orderSpinner').show();
        }
    });

    function plotRevenueChart (tableObject) {
        let completeData = tableObject.tableName.rows({search:'applied'}).data();
        let splitedValues = {};
        let sum = 0;
        let firstDate = moment(tableObject.table === 'orderTable' ? $('#ordFirstDate').val() : $('#renFirstDate').val());
        let lastDate = moment(tableObject.table === 'orderTable' ? $('#ordLastDate').val() : $('#renLastDate').val());
        let dateDiffernece = lastDate.diff(firstDate, 'months');
        let label = 'Revenue By Year';
        let filterBy = 'YYYY';
        let sumVal = '';
        let multiYear = false;

        if (firstDate && lastDate && dateDiffernece < 12) {
            filterBy = 'MM';
            multiYear =  moment(firstDate).format('YYYY') != moment(lastDate).format('YYYY') ? true : false;
            label = 'Revenue for the Year ' + moment(firstDate).format('YYYY') + (multiYear ? ' & ' +  moment(lastDate).format('YYYY') : '');
        }

        completeData.each((item) => {
            if (moment(item[tableObject.chart.invoiceDateName]).format(filterBy) in splitedValues) {
                splitedValues[moment(item[tableObject.chart.invoiceDateName]).format(filterBy)] += item[tableObject.chart.salesName];
            } else { 
                splitedValues[moment(item[tableObject.chart.invoiceDateName]).format(filterBy)] = item[tableObject.chart.salesName];
            }
        });

        tableObject.tableName.column(tableObject.chart.salesColumnNo, {search:'applied'}).data().each(function(value) {
            if (Number.isInteger(parseInt(value))) {
                sum += value;
            }
        });

        sumVal = 'Total Revenue $' + sum.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        drawChart(tableObject, splitedValues, sumVal, label, 'revenue', multiYear, lastDate);
    }

    function plotTotalOrderChart (tableObject) {
        let completeData = tableObject.tableName.rows({search:'applied'}).data();
        let splitedValues = {};
        let totalCount = tableObject.tableName.column(tableObject.chart.poColumnNo, {search:'applied'}).data().unique().length;
        let poObjects = [];
        let firstDate = moment(tableObject.table === 'orderTable' ? $('#ordFirstDate').val() : $('#renFirstDate').val());
        let lastDate = moment(tableObject.table === 'orderTable' ? $('#ordLastDate').val() : $('#renLastDate').val());
        let dateDiffernece = lastDate.diff(firstDate, 'months');
        let label = 'Orders By Year';
        let filterBy = 'YYYY';
        let multiYear = false;

        if (firstDate && lastDate && dateDiffernece < 12) {
            filterBy = 'MM';
            multiYear =  moment(firstDate).format('YYYY') != moment(lastDate).format('YYYY') ? true : false;
            label = 'Orders for the Year ' + moment(firstDate).format('YYYY') + (multiYear ? ' & ' +  moment(lastDate).format('YYYY') : '');
        }

        completeData.each((item) => {
            let newObj = filterBy === 'MM' ? item[tableObject.chart.poName] : moment(item[tableObject.chart.invoiceDateName]).format(filterBy) + item[tableObject.chart.poName];
            if (moment(item[tableObject.chart.invoiceDateName]).format(filterBy) in splitedValues && poObjects.indexOf(newObj) === -1) {
                poObjects.push(newObj);
                splitedValues[moment(item[tableObject.chart.invoiceDateName]).format(filterBy)] += 1;
            } else if (poObjects.indexOf(newObj) === -1) {
                poObjects.push(newObj);
                splitedValues[moment(item[tableObject.chart.invoiceDateName]).format(filterBy)] = 1;
            }
        });

        drawChart(tableObject, splitedValues, 'Total Orders ' + totalCount, label, 'order', multiYear, lastDate);
    }

    function drawChart (tableObject, splitedValues, sum, label, chartName, multiYear, lastDate) {
        let chartArray = chartName === 'order' ? [['Move', 'Orders']] : [['Move', 'Revenue']];
        let chartId = chartName === 'order' ? tableObject.chart.chartOrderId : tableObject.chart.chartRevenueId;
        let chartTitle = chartName === 'order' ? tableObject.chart.chartOrderTitle : tableObject.chart.chartRevenueTitle;
        let yearOrderChange = [];
        delete splitedValues['Invalid date'];
        for (var i in splitedValues)
            chartArray.push([i, splitedValues[i]]);

        chartArray = chartArray.sort(function(a, b) {
            return a[0] - b[0];
        });

        chartArray.forEach((item)=> {
            if (window.partnerInsights.months[item[0]] != undefined) {
                item[0] = window.partnerInsights.months[item[0]];
            }
        });

        if (multiYear) {
            yearOrderChange = chartArray.splice(1, Number(moment(lastDate).format('MM')));
            chartArray = chartArray.concat(yearOrderChange);
        }
    
        let data = new google.visualization.arrayToDataTable(chartArray);
        let options = {
            width: 275,
            legend: {position: 'none'},
            chart: {
                title: chartTitle,
                subtitle: sum
            },
            axes: {
                x: {
                    0: {side: 'bottom', label: label}
                }
            },
            bar: {groupWidth: '90%'}
        };
    
        let chart = new google.charts.Bar(document.getElementById(chartId));
        chart.draw(data, google.charts.Bar.convertOptions(options));
    }

    createDateAndFilterEvent('#renewalContainer', window.partnerInsights.renewalAjaxData);
    createDateAndFilterEvent('#orderContainer', window.partnerInsights.orderAjaxData);

    function createDateAndFilterEvent (container, data) {
        $(`${container} .filter-select, ${container} .date-selector`).on('change', function () {
            setTimeout(() => {
                plotRevenueChart(data);
                plotTotalOrderChart(data);
            }, 500);
        });
    }
});