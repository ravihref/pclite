window.partnerInsights = window.partnerInsights || {};
window.partnerInsights.numberOfRows = 50000;
window.partnerInsights.dateFormat = 'YYYY-MM-DD';
window.partnerInsights.host = "https://westconapidev.azure-api.net";
window.partnerInsights.filterAppendObject = {
    renPartner: '',
    renEndUser: '',
    ordPartner: '',
    ordEndUser: '',
};
window.partnerInsights.months = {
    '01': 'Jan',
    '02': 'Feb',
    '03': 'Mar',
    '04': 'Apr',
    '05': 'May',
    '06': 'June',
    '07': 'July',
    '08': 'Aug',
    '09': 'Sep',
    '10': 'Oct',
    '11': 'Nov',
    '12': 'Dec'
};

window.partnerInsights.renewalSelects = {
    selects: ['renCountry', 'renPartner', 'renEndUser', 'renPoNo', 'renContractNo'],
    inputs: ['renFirstDate', 'renLastDate']
};
window.partnerInsights.orderSelects = {
    selects: ['ordPartner', 'ordCountry', 'ordEndUser', 'ordPoNo', 'ordVendor'],
    inputs: ['ordFirstDate', 'ordLastDate']
};
window.partnerInsights.select2IdsAndPlaceholders = {
    renCountry: 'Country',
    renPartner: 'Partner',
    renEndUser: 'End-User',
    renPoNo: 'PO No.',
    renSerialNo: 'Serial No.',
    renContractNo: 'Contract No.',
    ordCountry: 'Country',
    ordPartner: 'Partner',
    ordEndUser: 'End-User',
    ordPoNo: 'PO No.',
    ordSku: 'SKU',
    ordVendor: 'Vendor'
};
window.partnerInsights.renewalHeaders = [
    {
        className: 'details-control',
        orderable: false,
        data: null,
        defaultContent: ''
    },
    { data: 'Country' },
    { data: 'Partner' },
    { data: 'EndUser' },
    { data: 'SalesOrder' },
    { data: 'CustomerPO' },
    { data: 'ProfitCenter' },
    { data: 'Sales'},
    { data: null, 
        render: function (data, type, full) {
            return data.FirstStartDate ? moment(data.FirstStartDate).format(window.partnerInsights.dateFormat) : '';
        }
    },
    { data: 'VendorContract' },
    { data: null, 
        render: function (data, type, full) {
            return data.ExpirationDate ? moment(data.ExpirationDate).format(window.partnerInsights.dateFormat) : '';
        }
    }
];
window.partnerInsights.orderHeaders = [
    {
        className: 'details-control',
        orderable: false,
        data: null,
        defaultContent: ''
    },
    { data: 'Country' },
    { data: 'Partner' },
    { data: 'EndUser' },
    { data: 'SalesOrder' },
    { data: 'CustomerPO' },
    { data: 'ProfitCenter' },
    { data: 'Sales'},
    { data: null, 
        render: function (data, type, full) {
            return data.InvoiceDate ? moment(data.InvoiceDate).format(window.partnerInsights.dateFormat) : '';
        }
    },
    { data: 'InvoiceMonth'}
];
window.partnerInsights.renewalAjaxData = {
    url: `${window.partnerInsights.host}/ordersrenewal/rv_partner_central_lite_renewals_header_rbac?$count=${window.partnerInsights.numberOfRows}&Email=ravi.teja@valtech.com`,
    table: 'renewalTable',
    chart: {
        chartRevenueTitle: 'Value of Renewals(KPI)',
        chartOrderTitle: 'Total Number of Renewals(KPI)',
        chartRevenueId: 'renRevenuChart',
        chartOrderId: 'renTotalChart',
        invoiceDateName: 'ExpirationDate',
        salesName: 'Sales',
        poName: 'CustomerPO',
        salesColumnNo: 7,
        poColumnNo: 5
    },
    spinner: 'renewalSpinner',
    error: 'renewalError',
    reload: 'renewalReload',
    tableName: '',
    columns: window.partnerInsights.renewalHeaders,
    container: 'renewalContainer',
    expansionEventType: 'renewal',
    date: {
        firstDate: 'renFirstDate',
        lastDate: 'renLastDate'
    },
    dropdown: [
        {
            id: 'renCountry',
            columnName: 'Country',
            columnNo: 1,
            linkedColumnId: 'renPartner',
            linkedColumnName: 'Partner',
            linkedColumnNo: 2
        },
        {
            id: 'renPartner',
            columnName: 'Partner',
            columnNo: 2,
            linkedColumnId: 'renEndUser',
            linkedColumnName: 'End-User',
            linkedColumnNo: 3
        },
        {
            id: 'renEndUser',
            columnName: 'End-User',
            columnNo: 3
        },
        {
            id: 'renPoNo',
            columnName: 'PO No.',
            columnNo: 5
        },
        {
            id: 'renContractNo',
            columnName: 'Contract No.',
            columnNo: 9
        }
    ]
};
window.partnerInsights.orderAjaxData = {
    url: `${window.partnerInsights.host}/ordersrenewal/rv_partner_central_lite_orders_header_rbac?$count=${window.partnerInsights.numberOfRows}&$orderby=InvoiceDate+DESC&Email=ravi.teja@valtech.com`,
    table: 'orderTable',
    chart: {
        chartRevenueTitle: 'Value of Orders(KPI)',
        chartOrderTitle: 'Number of Orders(KPI)',
        chartRevenueId: 'ordRevenuChart',
        chartOrderId: 'ordOrdersChart',
        invoiceDateName: 'InvoiceDate',
        salesName: 'Sales',
        poName: 'CustomerPO',
        salesColumnNo: 7,
        poColumnNo: 5
    },
    spinner: 'orderSpinner',
    error: 'orderError',
    reload: 'orderReload',
    tableName: '',
    columns: window.partnerInsights.orderHeaders,
    container: 'orderContainer',
    expansionEventType: 'order',
    date: {
        firstDate: 'ordFirstDate',
        lastDate: 'ordLastDate'
    },
    dropdown: [
        {
            id: 'ordCountry',
            columnName: 'Country',
            columnNo: 1,
            linkedColumnId: 'ordPartner',
            linkedColumnName: 'Partner',
            linkedColumnNo: 2
        },
        {
            id: 'ordPartner',
            columnName: 'Partner',
            columnNo: 2,
            linkedColumnId: 'ordEndUser',
            linkedColumnName: 'End-User',
            linkedColumnNo: 3
        },
        {
            id: 'ordEndUser',
            columnName: 'End-User',
            columnNo: 3
        },
        {
            id: 'ordPoNo',
            columnName: 'PO No.',
            columnNo: 5
        },
        {
            id: 'ordVendor',
            columnName: 'Vendor',
            columnNo: 6
        }
    ]
};